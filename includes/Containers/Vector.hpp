#ifndef VECTOR_HPP
#define VECTOR_HPP

#include "../../srcs/main.hpp"

namespace ft
{
	template < class T, class Alloc = std::allocator<T> >
    class vector
    {
    public:
        /* Aliases definition */
        typedef T                                               value_type;
        typedef Alloc                                           allocator_type;

        typedef value_type&                                     reference;
        typedef value_type*                                     pointer;

        typedef const value_type&                               const_reference;
        typedef const value_type*                               const_pointer;

        typedef RandomAccess<value_type, false>                 iterator;
        typedef RandomAccess<value_type, true>                  const_iterator;

        typedef RevRandomAccess<value_type, false>              reverse_iterator;
        typedef RevRandomAccess<value_type, true>               const_reverse_iterator;

        typedef long int                                        difference_type;
        typedef size_t                                          size_type;

    private:
        /* Attributs */
        pointer                                                 _data;          // pointer on an array of T values.
        size_type                                               _size;          // number of T values inside the vector.
        size_type                                               _capacity;      // capacity allocated - pattern -> when capacity == size then capacity = size * 2.
        allocator_type                                          _alloc;         // copy of allocator_type object.
    
    public:
        /* Constructors */
        explicit vector (const allocator_type& alloc = allocator_type());
        explicit vector (size_type n, const value_type& val = value_type(), const allocator_type& alloc = allocator_type());
        vector (iterator first, iterator last, const allocator_type& alloc = allocator_type());
        vector (const_iterator first, const_iterator last, const allocator_type& alloc = allocator_type());
        vector(const vector& x);
        /* Destructor */
        ~vector();
        /* Overloading operator */
        vector& operator = (const vector& assignObj);

    public:
        /* Iterators */
        const_iterator begin() const { return const_iterator(this->_data); }
        const_iterator end() const { return const_iterator(this->_data + this->_size); }
        iterator begin() { return iterator(this->_data); }
        iterator end() { return iterator(this->_data + this->_size); }

        const_reverse_iterator rbegin() const { return const_reverse_iterator(this->_data + (this->_size - 1)); }
        const_reverse_iterator rend() const { return const_reverse_iterator(this->_data - 1); }
        reverse_iterator rbegin() { return reverse_iterator(this->_data + (this->_size - 1)); }
        reverse_iterator rend() { return reverse_iterator(this->_data - 1); }
        
    public:
        /* Capacity */
        size_type size() const { return (this->_size); }
		size_type max_size() const	{ return (std::numeric_limits<size_type>::max() / (sizeof(value_type))); }
        void resize (size_type n, value_type val = value_type());
        size_type capacity() const { return (this->_capacity); }
        bool empty() const { return (this->_size == 0); }
        void reserve (size_type n);

    public: 
        /* Element access */
        reference operator[] (size_type n) { return (this->_data[n]); }
        const_reference operator[] (size_type n) const { return (this->_data[n]); }
        reference at (size_type n)
        {
            if (n >= this->_size)
                throw (std::out_of_range("out of range"));
            return (this->_data[n]);
        }
        const_reference at (size_type n) const { return (this->_data[n]); }
        reference front() { return (this->_data[0]); }
        const_reference front() const { return (this->_data[0]); }
        reference back() { return (this->_data[this->_size - 1]); }
        const_reference back() const { return (this->_data[this->_size - 1]); }

    public:
        /* Modifiers */
        void assign (iterator first, iterator last);
        void assign (const_iterator first, const_iterator last);
        void assign (size_type n, const value_type& val);
        void push_back (const value_type& val);
        void pop_back();
        iterator insert (iterator position, const value_type& val);
        void insert (iterator position, size_type n, const value_type& val);
        void insert (iterator position, iterator first, iterator last);
        void insert (iterator position, const_iterator first, const_iterator last);
        iterator erase (iterator position);
        iterator erase (iterator first, iterator last);
        void swap (vector& x);
        void clear();

    private:
        /* Reallocate Vector */
        void _reallocateVec(size_type newCapacity)
        {
            pointer tmp = this->_alloc.allocate(newCapacity);
            for (size_type i = 0; i < this->_size; ++i)
                this->_alloc.construct(&tmp[i], this->_data[i]);

            this->~vector();
            this->_capacity = newCapacity;
            this->_data = tmp;
        }

    }; // class vector

    /***** Class implementation *****/

    /*** Member functions ***/

    /* Constructors */
    template < class T, class Alloc >
    vector<T, Alloc>::vector(const allocator_type& alloc)
    {
        this->_size = 0;
        this->_capacity = 0;
        this->_alloc = alloc;
        this->_data = NULL;
    }

    template < class T, class Alloc >
    vector<T, Alloc>::vector (size_type n, const value_type& val, const allocator_type& alloc)
        : _size(n), _capacity(n), _alloc(alloc)
    {
        this->_data = this->_alloc.allocate(this->_capacity);
    
        for (size_type i = 0; i < this->_size; i++)
            this->_alloc.construct(&this->_data[i], val);
    }

    template < class T, class Alloc >
    vector<T, Alloc>::vector (iterator first, iterator last, const allocator_type& alloc)
        : _size(0), _alloc(alloc)
    {
        this->_size = (last - first);
        this->_capacity = this->_size;
        this->_data = this->_alloc.allocate(this->_capacity);
    
        for (int i = 0; first != last; ++first, ++i)
            this->_alloc.construct(&this->_data[i], *first);
    }

    template < class T, class Alloc >
    vector<T, Alloc>::vector(const_iterator first, const_iterator last, const allocator_type& alloc)
         : _size(0), _alloc(alloc)
    {
        this->_size = (last - first);
        this->_capacity = this->_size;
        this->_data = this->_alloc.allocate(this->_capacity);

        for (int i = 0; first != last; ++first, ++i)
            this->_alloc.construct(&this->_data[i], *first);
    }

    template < class T, class Alloc >
    vector<T, Alloc>::vector(const vector& x)
        : _size(x._size), _capacity(x._capacity), _alloc(x._alloc)
    {        
        this->_data = _alloc.allocate(this->_capacity);

        int i = 0;
        for (const_iterator it = x.begin() ; it != x.end() ; ++it)
            this->_alloc.construct(&this->_data[i++], *it);
    }

    template < class T, class Alloc >
    vector<T, Alloc>::~vector()
    {
        for (iterator it = begin(); it != end(); ++it)
            this->_alloc.destroy(&(*it));
    
        this->_alloc.deallocate(this->_data, this->_capacity);
    }

    /* Overloading operator = */
    template < class T, class Alloc >
    vector<T, Alloc>& vector<T, Alloc>::operator = (const vector& assignObj)
    {
        if (this->_data)
            this->_alloc.deallocate(this->_data, this->_capacity);
        this->_data = this->_alloc.allocate(assignObj._capacity);
        this->_capacity = assignObj._capacity;
        this->_size = 0;
        this->assign(assignObj.begin(), assignObj.end());
        return (*this);
    }

    /* Capacity */
    template < class T, class Alloc >
    void vector<T, Alloc>::resize (size_type n, value_type val)
    {
        while (n < this->_size)
            this->pop_back();
        if (n > this->_capacity)
            this->reserve(n);
        while (n > this->_size)
            this->push_back(val);
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::reserve (size_type n)
    {
        if (n > max_size())
            throw std::length_error("vector");
        if (n > this->_capacity)
            _reallocateVec(n);
    } 

    /* Modifiers */
    template < class T, class Alloc >
    void vector<T, Alloc>::assign (iterator first, iterator last)
    {
        this->clear();
        for (; first != last ; ++first)
            this->push_back(*first);
    }
    
    template < class T, class Alloc >
    void vector<T, Alloc>::assign (const_iterator first, const_iterator last)
    {
        this->clear();
        for (; first != last ; ++first)
            this->push_back(*first);
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::assign (size_type n, const value_type& val)
    {
        this->clear();
	    for (size_type i = 0; i < n ; i++)
            this->push_back(val);
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::push_back (const value_type& val)
    {
        if (this->_size + 1 > this->_capacity)
            _reallocateVec((this->_capacity == 0) ? 1 : this->_capacity * 2);

        this->_alloc.construct(&this->_data[this->_size++], val);
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::pop_back ()
    {
        if (this->_size)
            this->_alloc.destroy(&this->_data[_size-- - 1]);
    }

    template < class T, class Alloc >
    typename vector<T, Alloc>::iterator vector<T, Alloc>::insert (iterator position, const value_type& val)
    {
        size_type n = (position - this->begin());
        insert(position, 1, val);
        return (iterator(&this->_data[n]));
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::insert (iterator position, size_type n, const value_type& val)
    {
        vector<T, Alloc> temp(position, this->end());

        this->_size -= (this->end() - position);

        for (size_type i = 0; i < n; i++)
            this->push_back(val);

        iterator it = temp.begin();
        iterator ite = temp.end();

        for (; it != ite ; ++it)
            this->push_back(*it);
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::insert (iterator position, iterator first, iterator last)
    {
        vector<T, Alloc> temp(position, this->end());

        this->_size -= (this->end() - position);

        for (; first != last ; ++first)
            this->push_back(*first);

        iterator it = temp.begin();
        iterator ite = temp.end();

        for (; it != ite ; ++it)
            this->push_back(*it);
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::insert (iterator position, const_iterator first, const_iterator last)
    {
        vector<T, Alloc> temp(position, this->end());

        this->_size -= (this->end() - position);

        for (; first != last ; ++first)
            this->push_back(*first);

        iterator it = temp.begin();
        iterator ite = temp.end();

        for (; it != ite ; ++it)
            this->push_back(*it);
    }

    template < class T, class Alloc >
    typename vector<T, Alloc>::iterator vector<T, Alloc>::erase (iterator position)
    {
        iterator tmp(position);
        iterator ite = this->end() - 1;

        for (; position != ite ; ++position)
            *position = *(position + 1);
    
        this->_size--;

        return (tmp);
    }

    template < class T, class Alloc >
    typename vector<T, Alloc>::iterator vector<T, Alloc>::erase (iterator first, iterator last)
    {
        iterator tmp(last);
        iterator ite = this->end();

        for (; last != ite ; ++last, ++first)
            *first = *last;
    
        this->_size -= (last - first);

        return (tmp);
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::clear ()
    {
        while (this->_size)
            this->pop_back();
    }

    // // /*** Non-member function overloads ***/

    template <class T, class Alloc>
    bool operator == (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
    {
        if (lhs.size() != rhs.size())
            return false;

        typename ft::vector<T, Alloc>::const_iterator it_lhs = lhs.begin();
        typename ft::vector<T, Alloc>::const_iterator ite_lhs = lhs.end();
        typename ft::vector<T, Alloc>::const_iterator it_rhs = rhs.begin();
        typename ft::vector<T, Alloc>::const_iterator ite_rhs = rhs.end();

        for (; (it_lhs != ite_lhs) && (it_rhs != ite_rhs) ; ++it_lhs, ++it_rhs)
            if (*(it_lhs) != *(it_rhs))
                return (false);
        return (true);
    }

    template <class T, class Alloc>
    bool operator != (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
    {
        return ((lhs == rhs) ? false : true);
    }

    template <class T, class Alloc>
    bool operator < (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
    {
        typename ft::vector<T, Alloc>::const_iterator it_lhs = lhs.begin();
        typename ft::vector<T, Alloc>::const_iterator ite_lhs = lhs.end();
        typename ft::vector<T, Alloc>::const_iterator it_rhs = rhs.begin();
        typename ft::vector<T, Alloc>::const_iterator ite_rhs = rhs.end();

        for (; (it_lhs != ite_lhs) && (it_rhs != ite_rhs) ; ++it_lhs, ++it_rhs)
            if (*(it_lhs) < *(it_rhs))
                return (true);
        return (lhs.size() < rhs.size());

    }

    template<class T, class Alloc>
    bool operator <= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
    {
        return (!(rhs < lhs));
    }

    template <class T, class Alloc>
    bool operator > (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
    {
        return (rhs < lhs);
    }

    template <class T, class Alloc>
    bool operator >= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
    {
        return (!(lhs < rhs));
    }

    template < class T, class Alloc >
    void vector<T, Alloc>::swap (vector& x)
    {
        vector<T, Alloc> temp = *this;
        *this = x;
        x = temp;
    }

} // namespace ft

#endif

// https://www.journaldev.com/37622/vector-insert-in-c-plus-plus
// https://www.cplusplus.com/reference/vector/vector/
// https://codereview.stackexchange.com/questions/180058/a-vector-implementation
// https://www.studytonight.com/cpp/stl/stl-container-vector
// https://codereview.stackexchange.com/questions/180058/a-vector-implementation
// https://www.youtube.com/watch?v=YpNCBw-cPWw
