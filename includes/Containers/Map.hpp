#ifndef MAP_HPP
#define MAP_HPP

#include "../../srcs/main.hpp"

namespace ft
{

	/*
		Il s'agit d'une surcharge de l'algorithme générique swap qui améliore 
		ses performances en transférant mutuellement la propriété de leurs actifs 
		à l'autre conteneur (c'est-à-dire que les conteneurs échangent des références 
		à leurs données, sans effectuer réellement de copie ou de déplacement d'éléments)
	*/
	template <typename T>
	void	swap(T &a, T &b)
	{
		T tmp = a;
		a = b;
		b = tmp;
	}

	template <class Key, class T, class Compare=std::less<Key>, class Alloc = std::allocator<std::pair<const Key, T> > >
	class map
	{
	    public:
			typedef Key 																		    key_type;
			typedef T 																			    mapped_type;
			typedef std::pair<const key_type, mapped_type> 										    value_type;
			typedef Compare 																	    key_compare;

			typedef Alloc 																		    allocator_type;
			typedef T& 																			    reference;
			typedef const T& 																	    const_reference;
			typedef T* 																			    pointer;
			typedef const T* 																	    const_pointer;

			typedef size_t 																		    size_type;

			typedef b_node<key_type, mapped_type>* 												    node;

			typedef map_iterator<key_type, mapped_type, pointer, reference> 						iterator;
			typedef map_iterator<key_type, mapped_type, const_pointer, const_reference> 			const_iterator;

			typedef reverse_map_iterator<key_type, mapped_type, pointer, reference> 				reverse_iterator;
			typedef reverse_map_iterator<key_type, mapped_type, const_pointer, const_reference>	    const_reverse_iterator;

			class value_compare : public std::binary_function<value_type, value_type, bool>
			{// in C++98, it is required to inherit binary_function<value_type,value_type,bool>
				friend class map;
				protected:
					Compare comp;
					value_compare (Compare c) : comp(c) {} // constructed with map's comparison object
				public:
					typedef bool result_type;
					typedef value_type first_argument_type;
					typedef value_type second_argument_type;
					bool operator() (const value_type& x, const value_type& y) const
					{
						return comp(x.first, y.first);
					}
			};

		private:
			allocator_type      _alloc;
			key_compare         _comp;
			node                _root;
			size_type           _nbNode;

			node _newNode(key_type key, mapped_type value, node parent, bool end = false)
			{
				node temp = new b_node<key_type, mapped_type>();
				temp->content = std::make_pair(key, value);
				temp->right = NULL;
				temp->left = NULL;
				temp->parent = parent;
				temp->end = end;
				return (temp);
			}
			void _treeCleaner(node tree)
			{
                if (!tree)
                    return ;
				_treeCleaner(tree->right);
				_treeCleaner(tree->left);
				delete tree;
			}
            
			node _nodeInsert(node n, key_type key, mapped_type value, bool end = false)
			{
				if (n->end)
				{
					if (!n->left)
					{
						n->left = _newNode(key, value, n, end);
						return (n->left);
					}
					return (_nodeInsert(n->left, key, value));
				}
				if (key < n->content.first && !end)
				{
					if (!n->left)
					{
						n->left = _newNode(key, value, n, end);
						return (n->left);
					}
					else
						return (_nodeInsert(n->left, key, value));
				}
				else
				{
					if (!n->right)
					{
						n->right = _newNode(key, value, n, end);
						return (n->right);
					}
					else
						return(_nodeInsert(n->right, key, value));
				}
			}

			node _keyFinder(node n, key_type key) const
			{
				node temp;
				if (!n->end && n->content.first == key && n->parent)
					return (n);
				if (n->right)
				{
					if ((temp = _keyFinder(n->right, key)))
						return (temp);
				}
				if (n->left)
				{
					if ((temp = _keyFinder(n->left, key)))
						return (temp);
				}
				return (0);
			}

			void _nodeRemover(node n)
			{
				node parent = n->parent;
				if (!n->left && !n->right)
				{
					if (parent->right == n)
						parent->right = 0;
					else
						parent->left = 0;
					delete n;
					return ;
				}
				if (n->right && !n->left)
				{
					if (parent->right == n)
						parent->right = n->right;
					else
						parent->left = n->right;
					n->right->parent = parent;
					delete n;
					return ;
				}
				if (n->left && !n->right)
				{
					if (parent->right == n)
						parent->right = n->left;
					else
						parent->left = n->left;
					n->left->parent = parent;
					delete n;
					return ;
				}
				node next = (++iterator(n)).node();
				if (!next)
					next = (--iterator(n)).node();

				ft::swap(next->content, n->content);
				_nodeRemover(next);
			}

			void _treeInitializer(void)
			{
				_root = _newNode(key_type(), mapped_type(), 0);
				_root->right = _newNode(key_type(), mapped_type(), _root, true);
				_nbNode = 0;
			}

			node _end(void) const
			{
				return (_root->right);
			}


		public:
			//Main
			explicit map(const key_compare &comp = key_compare(), const allocator_type alloc = allocator_type());
			map(const map<Key, T> &other);
			~map();
			map &operator=(const map<Key, T> &other);

			template <class InputIterator>
			map(InputIterator first, InputIterator last, const key_compare &comp = key_compare(), const allocator_type alloc = allocator_type())
			{
				this->_alloc = alloc;
				this->_comp = comp;
				this->_treeInitializer();
				this->insert(first, last);
			}

			//Iterators
			iterator begin();
			const_iterator begin() const;
			
			iterator		end()		{ return (iterator(this->_end()));}
			const_iterator	end() const	{ return (const_iterator(this->_end()));}

			reverse_iterator rbegin();
			const_reverse_iterator rbegin() const;
			
			reverse_iterator		rend()			{ return (reverse_iterator(this->_root));}
			const_reverse_iterator	rend() const	{ return (const_reverse_iterator(this->_root));}

			//Capacity
			bool		empty() const		{ return (this->_nbNode == 0);}
			size_type	size() const		{ return (this->_nbNode);}
			size_type	max_size() const	{ return (std::numeric_limits<size_type>::max() / (sizeof(b_node<key_type, mapped_type>)));}

			//Element access
			mapped_type &operator[](const key_type& k);
			
			//Modifiers
			std::pair<iterator, bool> insert(const value_type &value);
			iterator insert(iterator position, const value_type &value);
			void erase(iterator position);
			size_type erase(const key_type &value);
			void erase(iterator first, iterator last);
			void swap(map &x);
			void clear();
			
			template <class InputIterator>
			void insert(InputIterator first, InputIterator last)
			{
				for (; first != last ; ++first)
					this->insert(*first);
			}

			//Observers
			key_compare		key_comp(void) const	{ return (_comp);}
			value_compare	value_comp(void) const	{ return (value_compare(_comp));}

			//Operations
			iterator find(const key_type &value);
			const_iterator find(const key_type &value) const;
			size_type count(const key_type &value) const;
			iterator lower_bound(const key_type &key);
			const_iterator lower_bound(const key_type &key) const;
			iterator upper_bound(const key_type &key);
			const_iterator upper_bound(const key_type &key) const;

			std::pair<const_iterator, const_iterator> equal_range(const key_type &k) const
			{
				return (std::pair<const_iterator, const_iterator>(this->lower_bound(k), this->upper_bound(k)));
			}
			std::pair<iterator, iterator> equal_range(const key_type &k)
			{
				return (std::pair<iterator, iterator>(this->lower_bound(k), this->upper_bound(k)));
			}
	};

/* Constructor */

template <class Key, class T, class Compare, class Alloc >
map<Key, T, Compare, Alloc>::map(const key_compare &comp, const allocator_type alloc)
{
	this->_alloc = alloc;
	this->_comp = comp;
	this->_treeInitializer();
}

template <class Key, class T, class Compare, class Alloc >
map<Key, T, Compare, Alloc>::map(const map<Key, T> &other)
{
	this->_treeInitializer();
	*this = other;
}

template <class Key, class T, class Compare, class Alloc >
map<Key, T, Compare, Alloc>::~map()
{
	this->_treeCleaner(this->_root);	
    this->_root = NULL;
}

template <class Key, class T, class Compare, class Alloc >
map<Key, T, Compare, Alloc> &map<Key, T, Compare, Alloc>::operator=(const map<Key, T> &other)
{
	this->clear();
	this->insert(other.begin(), other.end());
	return (*this);
}

/* Iterators */

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::iterator map<Key, T, Compare, Alloc>::begin()
{
	node n = this->_root;
	if (!n->left && !n->right)
		return (this->end());
	if (!n->left && n->right)
		n = n->right;
	while (n->left)
		n = n->left;
	return (iterator(n));
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::const_iterator map<Key, T, Compare, Alloc>::begin() const
{
	node n = this->_root;
	if (!n->left && !n->right)
		return (this->end());
	if (!n->left && n->right)
		n = n->right;
	while (n->left)
		n = n->left;
	return (const_iterator(n));
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::reverse_iterator map<Key, T, Compare, Alloc>::rbegin()
{
	iterator i = this->end();
	i--;
	return (reverse_iterator(i.node()));
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::const_reverse_iterator map<Key, T, Compare, Alloc>::rbegin() const
{
	iterator i = this->end();
	i--;
	return (const_reverse_iterator(i.node()));
}

/* Element access */

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::mapped_type &map<Key, T, Compare, Alloc>::operator[](const key_type& k)
{
	iterator tmp = this->find(k);
	if (tmp != this->end())
		return tmp->second;
	return (this->insert(std::make_pair(k, mapped_type())).first->second);
}

/* Modifiers */

template <class Key, class T, class Compare, class Alloc >
std::pair<typename map<Key, T, Compare, Alloc>::iterator, bool> map<Key, T, Compare, Alloc>::insert(const value_type &value)
{
	iterator temp;
	if ((temp = this->find(value.first)) != this->end())
		return (std::make_pair(temp, false));
	++this->_nbNode;
	return (std::make_pair(iterator(this->_nodeInsert(this->_root, value.first, value.second)), true));
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::iterator  map<Key, T, Compare, Alloc>::insert(iterator position, const value_type &value)
{
	iterator temp;
	if ((temp = this->find(value.first)) != this->end())
		return (temp);
	++this->_nbNode;
	return (iterator(_nodeInsert(position.node(), value.first, value.second)));
}

template <class Key, class T, class Compare, class Alloc >
void map<Key, T, Compare, Alloc>::erase(iterator position)
{
	this->_nodeRemover(position.node());
	--this->_nbNode;
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::size_type map<Key, T, Compare, Alloc>::erase(const key_type &value)
{
	size_type i = 0;
	iterator item;
	while ((item = this->find(value)) != this->end())
	{
		this->erase(item);
		++i;
	}
	return (i);
}

template <class Key, class T, class Compare, class Alloc >
void map<Key, T, Compare, Alloc>::erase(iterator first, iterator last)
{
	while (first != last)
		this->erase(first++);
}

template <class Key, class T, class Compare, class Alloc >
void map<Key, T, Compare, Alloc>::swap(map &x)
{
	map<Key, T, Compare, Alloc> temp = *this;
	*this = x;
	x = temp;
}

template <class Key, class T, class Compare, class Alloc >
void map<Key, T, Compare, Alloc>::clear()
{
	this->erase(this->begin(), this->end());
}

/* Operation */
template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::iterator map<Key, T, Compare, Alloc>::find(const key_type &value)
{
	if (this->empty())
		return (this->end());
	node temp = this->_keyFinder(this->_root, value);
	if (temp)
		return (iterator(temp));
	return (this->end());
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::const_iterator map<Key, T, Compare, Alloc>::find(const key_type &value) const
{
	if (this->empty())
		return (this->end());
	node temp = this->_keyFinder(this->_root, value);
	if (temp)
		return (const_iterator(temp));
	return (this->end());
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::size_type map<Key, T, Compare, Alloc>::count(const key_type &value) const
{
	const_iterator it = this->begin();
	const_iterator ite = this->end();
	for (; it != ite ; ++ it)
		if (it->first == value)
			return (1);
	return (0);
}

/*
	Lower bound (limite inférieure)
	Renvoie un itérateur pointant sur le premier élément du conteneur dont la clé n'est pas considérée 
	comme postérieure à key.
	Renvoie un itérateur vers le premier élément pour lequel key_comp(element_key,k) renverrait false
*/

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::iterator map<Key, T, Compare, Alloc>::lower_bound(const key_type &key)
{
	iterator it = this->begin();
	iterator ite = this->end();
	for (; it != ite ; ++it)
		if (this->_comp(it->first, key) == false)
			return (it);
	return (ite);
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::const_iterator map<Key, T, Compare, Alloc>::lower_bound(const key_type &key) const
{
	const_iterator it = this->begin();
	const_iterator ite = this->end();
	for (; it != ite ; ++it)
		if (this->_comp(it->first, key) == false) 
			return (it);
	return (ite);
}

/*
	Upper bound (limite supérieure)
	Renvoie un itérateur pointant sur le premier élément du conteneur dont la clé n'est pas considérée 
	comme antérieure à key.
	Renvoie un itérateur vers le premier élément pour lequel key_comp(element_key,k) renverrait false
*/

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::iterator map<Key, T, Compare, Alloc>::upper_bound(const key_type &key)
{
	iterator it = this->begin();
	iterator ite = this->end();
	for (; it != ite ; ++it)
		if (it->first != key && this->_comp(it->first, key) == false)
			return (it);
	return (ite);
}

template <class Key, class T, class Compare, class Alloc >
typename map<Key, T, Compare, Alloc>::const_iterator map<Key, T, Compare, Alloc>::upper_bound(const key_type &key) const
{
	const_iterator it = this->begin();
	const_iterator ite = this->end();
	for (; it != ite ; ++it)
		if (it->first != key && this->_comp(it->first, key) == false)
			return (it);
	return (ite);
}

} //namespace ft

// https://docs.microsoft.com/fr-fr/cpp/standard-library/map-class?view=msvc-160
// https://cplusplus.com/reference/map/map/?kw=map
// https://www.youtube.com/watch?v=lhTCSGRAlXI&list=PLpPXw4zFa0uKKhaSz87IowJnOTzh9tiBk&index=43
// https://www.youtube.com/watch?v=jDM6_TnYIqE
// https://www.youtube.com/c/SimpleSnippets/search?query=avl
// https://www.cs.odu.edu/~zeil/cs361/latest/Public/treetraversal/index.html
// https://courses.cs.vt.edu/~cs3114/Fall17/barnette/notes/Tree-Iterators.pdf
// https://www.labunix.uqam.ca/~malenfant_b/inf3105/inf3105-notes.pdf
// https://www.youtube.com/c/SimpleSnippets/search?query=avl
// https://www.youtube.com/watch?v=YWqla0UX-38
// https://fr.wikipedia.org/wiki/Arbre_binaire_de_recherche
// https://fr.wikipedia.org/wiki/Rotation_d%27un_arbre_binaire_de_recherche
// https://fr.wikipedia.org/wiki/Arbre_AVL
// https://www.labri.fr/perso/maylis/ASDF/supports/notes/AVL.html
// https://cours.etsmtl.ca/SEG/FHenri/inf145/Suppl%C3%A9ments/arbres%20AVL.htm

#endif
