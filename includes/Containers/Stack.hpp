#ifndef STACK_HPP
#define STACK_HPP

#include "../../srcs/main.hpp"

namespace ft
{
    template <class T, class Container = vector<T> >
    class stack
    {
    public:
        /* Aliases definition */
        typedef T                   value_type;
        typedef value_type&         reference;
        typedef const value_type&   const_reference;

        typedef Container           container_type;
        typedef size_t              size_type;

    public:
        /* Attributs */
        container_type      _container;
        /* Default constructor */
        explicit stack (const container_type& ctnr = container_type()) : _container(ctnr) {}
        /* Methods */
        bool empty() const { return (_container.empty()); }
        size_type size() const { return (_container.size()); }

        reference top() { return (_container.back()); }
        const_reference top() const { return (_container.back()); }
        
        void push (const_reference val) { _container.push_back(val); }
        void pop() { _container.pop_back(); }

    }; // class stack

    template <class T, class Container>
    bool operator==(const stack<T, Container>& lhs, const stack<T, Container>& rhs) { return (lhs._container == rhs._container);}

    template <class T, class Container>
    bool operator!=(const stack<T, Container>& lhs, const stack<T, Container>& rhs) { return (lhs._container != rhs._container);}

    template <class T, class Container>
    bool operator<(const stack<T, Container>& lhs, const stack<T, Container>& rhs) { return (lhs._container < rhs._container);}

    template <class T, class Container>
    bool operator>(const stack<T, Container>& lhs, const stack<T, Container>& rhs) { return (lhs._container > rhs._container);}

    template <class T, class Container>
    bool operator<=(const stack<T, Container>& lhs, const stack<T, Container>& rhs) { return (lhs._container <= rhs._container);}

    template <class T, class Container>
    bool operator>=(const stack<T, Container>& lhs, const stack<T, Container>& rhs) { return (lhs._container >= rhs._container);}

} // namespace ft

#endif
