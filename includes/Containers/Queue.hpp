#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "../../srcs/main.hpp"

namespace ft
{
    template <class T, class Container = list<T> >
    class queue
    {
    public:
        /* Aliases definition */
        typedef T                   value_type;

        typedef value_type&         reference;
        typedef const value_type&   const_reference;

        typedef Container           container_type;
        typedef size_t              size_type;

    public:
        /* Attributs */
        container_type      _container;
        /* Default constructor */
        explicit queue (const container_type& ctnr = container_type()) : _container(ctnr) {}
        /* Methods */
        bool empty() const { return (_container.empty()); }
        size_type size() const { return (_container.size()); }

        reference front() { return (_container.front()); }
	    const_reference front() const{ return (_container.front()); }

		reference back() { return (_container.back()); }
		const_reference back() const { return (_container.back()); }

        void push (const_reference val) { _container.push_back(val); }
        void pop() { _container.pop_back(); }

    }; // class queue

    template <class T, class Container>
    bool operator==(const queue<T, Container>& lhs, const queue<T, Container>& rhs) { return (lhs._container == rhs._container);}

    template <class T, class Container>
    bool operator!=(const queue<T, Container>& lhs, const queue<T, Container>& rhs) { return (lhs._container != rhs._container);}

    template <class T, class Container>
    bool operator<(const queue<T, Container>& lhs, const queue<T, Container>& rhs) { return (lhs._container < rhs._container);}

    template <class T, class Container>
    bool operator>(const queue<T, Container>& lhs, const queue<T, Container>& rhs) { return (lhs._container > rhs._container);}

    template <class T, class Container>
    bool operator<=(const queue<T, Container>& lhs, const queue<T, Container>& rhs) { return (lhs._container <= rhs._container);}

    template <class T, class Container>
    bool operator>=(const queue<T, Container>& lhs, const queue<T, Container>& rhs) { return (lhs._container >= rhs._container);}

} // namespace ft

#endif
