#ifndef BIDIRECTIONAL_HPP
#define BIDIRECTIONAL_HPP

#include "../../srcs/main.hpp"

// @param T     Type of container's elements.
// @param B     Boolean to indicate if it's an iterator / a const iterator.

template <typename T, bool B>
class  Bidirectional
{
public:
/* Aliases definition */
    typedef long int                                        difference_type;            // distance or range lenght between iterator in a container and another one
    typedef size_t                                          size_type;
    typedef T                                               value_type;                 // ex : int, float, double ..etc
    typedef typename chooseConst<B, T*, const T*>::type     pointer;
    typedef typename chooseConst<B, T&, const T&>::type     reference;
    typedef Node<T>*                                        node_pointer;

private:
/* Attributs */
    node_pointer                                            _current;

public:
/* Constructors */
    Bidirectional(node_pointer initLocation = 0) : _current(initLocation) {}

/* Destructor */
    ~Bidirectional() {}

/* Copy constructor */
    node_pointer getCurrent() const { return (this->_current); }
    Bidirectional(const Bidirectional<value_type, false>& copyObj) { this->_current = copyObj.getCurrent(); }

/* Overloading operators assignation */
    Bidirectional& operator = (const Bidirectional& assignObj)
    {
        if (this != &assignObj)
            this->_current = assignObj.getCurrent();
        return (*this);
    }

/* Overloading operators dereferencement */
    reference operator * () { return this->_current->data; }
    pointer operator -> () { return &this->_current->data; }

/* Overloading operators comparaison */
    bool operator == (const Bidirectional& other) const { return (this->_current == other._current); }
    bool operator != (const Bidirectional& other) const { return (this->_current != other._current); }

/* Overloading operators decrementation */
    Bidirectional& operator -- ()
    { 
        this->_current = this->_current->prev; 
        return (*this);
    }   // postfix
    Bidirectional operator -- (int)
    {
        Bidirectional oldCopy = *this;
        --(*this);
        return (oldCopy);
    }   // prefix
    
/* Overloading operators incrementation */
    Bidirectional& operator ++ ()
    {
        this->_current = this->_current->next; 
        return (*this);
    }   // postfix
    Bidirectional operator ++ (int)
    {
        Bidirectional oldCopy = *this;
        ++(*this);
        return (oldCopy);
    }   // prefix

}; // class Bidirectional


template <typename T, bool B>
class  RevBidirectional
{
public:
/* Aliases definition */
    typedef long int                                        difference_type;            // distance or range lenght between iterator in a container and another one
    typedef size_t                                          size_type;
    typedef T                                               value_type;                 // ex : int, float, double ..etc
    typedef typename chooseConst<B, T*, const T*>::type     pointer;
    typedef typename chooseConst<B, T&, const T&>::type     reference;
    typedef Node<T>*                                        node_pointer;

private:
/* Attributs */
    node_pointer                                            _current;

public:
/* Constructors */
    RevBidirectional(node_pointer initLocation = 0) : _current(initLocation) {}

/* Destructor */
    ~RevBidirectional() {}

/* Copy constructor */
    node_pointer getCurrent() const { return (this->_current); }
    RevBidirectional(const Bidirectional<value_type, false>& copyObj) { _current = copyObj.getCurrent(); }

/* Overloading operators assignation */
    RevBidirectional& operator = (const RevBidirectional& assignObj)
    {
        if (this != &assignObj)
            this->_current = assignObj.getCurrent();
        return (*this);
    }

/* Overloading operators dereferencement */
    reference operator * () { return this->_current->data; }
    pointer operator -> () { return &this->_current->data; }

/* Overloading operators comparaison */
    bool operator == (const RevBidirectional& other) const { return (this->_current == other._current); }
    bool operator != (const RevBidirectional& other) const { return (this->_current != other._current); }

/* Overloading operators decrementation */
    RevBidirectional& operator -- ()
    { 
        this->_current = this->_current->next; 
        return (*this);
    }   // postfix
    RevBidirectional operator -- (int)
    {
        RevBidirectional oldCopy = *this;
        ++(*this);
        return (oldCopy);
    }   // prefix
    
/* Overloading operators incrementation */
    RevBidirectional& operator ++ ()
    {
        this->_current = this->_current->prev; 
        return (*this);
    }   // postfix
    RevBidirectional operator ++ (int)
    {
        RevBidirectional oldCopy = *this;
        --(*this);
        return (oldCopy);
    }   // prefix

}; // class RevBidirectional

#endif