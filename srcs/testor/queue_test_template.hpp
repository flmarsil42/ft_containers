#ifndef QUEUE_TEST_TEMPLATE_HPP
#define QUEUE_TEST_TEMPLATE_HPP

#include "../main.hpp"

template <typename T>
bool queue_check_constructor(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_constructor: \n" << RESET;
    os << "\n~ queue (const container_type& ctnr) :\n\n";
    os << "\n~ Empty constructor:\n\n";
    std::queue<T> queue1;
    ft::queue<T> ft_queue1;

    os << "\n[queue] :\t\t" << queue1.size() << std::endl;
    os << "[ft_queue] :\t\t" << ft_queue1.size() << std::endl;

    if (queue1.size() != ft_queue1.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty constructor with list x3\n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);

    std::queue<T, std::list<T> > queue2(list);
    ft::queue<T, ft::list<T> > ft_queue2(ft_list);

    os << "\n[queue.size] :\t\t" << queue2.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue2.size() << std::endl;

    if (queue2.size() != ft_queue2.size())
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    *p_log << os.str();
    return (true);
}

template <typename T>
bool queue_check_empty(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_empty: \n" << RESET;
    os << "\nCreaty queue empty \n";

    std::queue<T> queue1;
    ft::queue<T> ft_queue1;

    os << "\n[queue.empty] :\t\t" << queue1.empty() << std::endl;
    os << "[ft_queue.empty] :\t" << ft_queue1.empty() << std::endl;

    if (queue1.empty() != ft_queue1.empty())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_a \n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);

    std::queue<T, std::list<T> > queue2(list);
    ft::queue<T, ft::list<T> > ft_queue2(ft_list);

    os << "\n[queue.empty] :\t\t" << queue2.empty() << std::endl;
    os << "[ft_queue.empty] :\t" << ft_queue2.empty() << std::endl;

    if (queue2.empty() != ft_queue2.empty())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_b \n";

    list.assign(12, data_b);
    ft_list.assign(12, data_b);

    std::queue<T, std::list<T> > queue3(list);
    ft::queue<T, ft::list<T> > ft_queue3(ft_list);

    os << "\n[queue.empty] :\t\t" << queue3.empty() << std::endl;
    os << "[ft_queue.empty] :\t" << ft_queue3.empty() << std::endl;

    if (queue3.empty() != ft_queue3.empty())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool queue_check_size(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_size: \n" << RESET;
    os << "\nCreaty queue empty\n";

    std::queue<T> queue1;
    ft::queue<T> ft_queue1;

    os << "\n[queue.size] :\t\t" << queue1.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue1.size() << std::endl;

    if (queue1.size() != ft_queue1.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_a\n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);

    std::queue<T, std::list<T> > queue2(list);
    ft::queue<T, ft::list<T> > ft_queue2(ft_list);

    os << "\n[queue.size] :\t\t" << queue2.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue2.size() << std::endl;

    if (queue2.size() != ft_queue2.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_b \n";

    list.assign(30, data_b);
    ft_list.assign(30, data_b);

    std::queue<T, std::list<T> > queue3(list);
    ft::queue<T, ft::list<T> > ft_queue3(ft_list);

    os << "\n[queue.size] :\t\t" << queue3.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue3.size() << std::endl;

    if (queue3.size() != ft_queue3.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool queue_check_front(std::ofstream *out, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_front: \n" << RESET;

    os << "\nCreaty queue with list -> data_a \n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);

    std::queue<T, std::list<T> > queue2(list);
    ft::queue<T, ft::list<T> > ft_queue(ft_list);

    os << "\n[queue] => " << queue2.front() << std::endl;
    os << "[ft_queue] => " << ft_queue.front() << std::endl;

    if (queue2.front() != ft_queue.front())
    {
        *out << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_b \n";

    list.assign(10, data_b);
    ft_list.assign(10, data_b);

    std::queue<T, std::list<T> > queue3(list);
    ft::queue<T, ft::list<T> > ft_queue3(ft_list);

    os << "\n[queue] => " << queue3.front() << std::endl;
    os << "[ft_queue] => " << ft_queue3.front() << std::endl;

    if (queue3.front() != ft_queue3.front())
    {
        *out << os.str();
        return (false);
    }
    
    *out << os.str();
    return (true);
}

template<typename T>
bool queue_check_back(std::ofstream *out, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_back: \n" << RESET;

    os << "\nCreaty queue with list -> data_a \n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);

    std::queue<T, std::list<T> > queue2(list);
    ft::queue<T, ft::list<T> > ft_queue(ft_list);

    os << "\n[queue2] => " << queue2.back() << std::endl;
    os << "[ft_queue] => " << ft_queue.back() << std::endl;

    if (queue2.back() != ft_queue.back())
    {
        *out << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_b \n";

    list.assign(10, data_b);
    ft_list.assign(10, data_b);

    std::queue<T, std::list<T> > queue3(list);
    ft::queue<T, ft::list<T> > ft_queue3(ft_list);

    os << "\n[queue3] => " << queue3.back() << std::endl;
    os << "[ft_queue3] => " << ft_queue3.back() << std::endl;

    if (queue3.back() != ft_queue3.back())
    {
        *out << os.str();
        return (false);
    }
    
    *out << os.str();
    return (true);
}


template<typename T>
bool queue_check_top(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_top: \n" << RESET;
    os << "\nCreaty queue with list -> data_a\n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);

    std::queue<T, std::list<T> > queue2(list);
    ft::queue<T, ft::list<T> > ft_queue2(ft_list);

    os << "\n[queue.size] :\t\t" << queue2.top() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue2.top() << std::endl;

    if (queue2.top() != ft_queue2.top())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_b \n";

    list.assign(7, data_b);
    ft_list.assign(7, data_b);

    std::queue<T, std::list<T> > queue3(list);
    ft::queue<T, ft::list<T> > ft_queue3(ft_list);

    os << "\n[queue.size] :\t\t" << queue3.top() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue3.top() << std::endl;

    if (queue3.top() != ft_queue3.top())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool queue_check_pop(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_pop: \n" << RESET;
    os << "\nCreaty queue with list -> data_a\n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);

    std::queue<T, std::list<T> > queue2(list);
    ft::queue<T, ft::list<T> > ft_queue2(ft_list);

    os << "\n[queue.size] :\t\t" << queue2.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue2.size() << std::endl;
    
    if (queue2.size() != ft_queue2.size())
    {
        *p_log << os.str();
        return (false);
    }

    queue2.pop();
    ft_queue2.pop();
    os << "\n[queue.size] :\t\t" << queue2.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue2.size() << std::endl;

    if (queue2.size() != ft_queue2.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    queue2.pop();
    ft_queue2.pop();
    os << "\n[queue.size] :\t\t" << queue2.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue2.size() << std::endl;

    if (queue2.size() != ft_queue2.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_b \n";

    list.assign(10, data_b);
    ft_list.assign(10, data_b);

    std::queue<T, std::list<T> > queue3(list);
    ft::queue<T, ft::list<T> > ft_queue3(ft_list);

    os << "\n[queue.size] :\t\t" << queue3.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue3.size() << std::endl;
    
    if (queue3.size() != ft_queue3.size())
    {
        *p_log << os.str();
        return (false);
    }

    queue3.pop();
    ft_queue3.pop();
    os << "\n[queue.size] :\t\t" << queue3.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue3.size() << std::endl;

    if (queue3.size() != ft_queue3.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    queue3.pop();
    ft_queue3.pop();
    os << "\n[queue.size] :\t\t" << queue3.size() << std::endl;
    os << "[ft_queue.size] :\t" << ft_queue3.size() << std::endl;

    if (queue3.size() != ft_queue3.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool queue_check_push(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_push: \n" << RESET;
    os << "\nCreaty queue with list -> data_a data_b \n";

    std::queue<T> queue;
    ft::queue<T> ft_queue;
    
    for (size_t i = 0; i < 5; i++)
    {
        queue.push(data_a);
        ft_queue.push(data_a);
    }

    os << "\nAfter push -> data_a\n";
    os << "\n[queue] => " << queue.size() << std::endl;
    os << "[ft_queue] => " << ft_queue.size() << std::endl;

    if (queue.size() != ft_queue.size())
    {
        *p_log << os.str();
        return (false);
    }

    for (size_t i = 0; i < 10; i++)
    {
        queue.push(data_b);
        ft_queue.push(data_b);
    }
    
    os << "\nAfter push -> data_b\n";
    os << "\n[queue] => " << queue.size() << std::endl;
    os << "[ft_queue] => " << ft_queue.size() << std::endl;

    if (queue.size() != ft_queue.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}


template<typename T>
bool queue_check_relational(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 queue_check_relational: \n" << RESET;
    os << "\nCreaty queue empty\n";

    std::queue<T> queue1;
    std::queue<T> queue2;
    ft::queue<T> ft_queue1;
    ft::queue<T> ft_queue2;

    os << "\n[queue]: After operator== " << (queue1 == queue2) << std::endl;
    os << "\n[ft_queue]: After operator== " << (ft_queue1 == ft_queue2) << std::endl;
    if ((queue1 == queue2) != (ft_queue1 == ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator!= " << (queue1 != queue2) << std::endl;
    os << "\n[ft_queue]: After operator!= " << (ft_queue1 != ft_queue2) << std::endl;
    if ((queue1 != queue2) != (ft_queue1 != ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator< " << (queue1 < queue2) << std::endl;
    os << "\n[ft_queue]: After operator< " << (ft_queue1 < ft_queue2) << std::endl;
    if ((queue1 < queue2) != (ft_queue1 < ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator<= " << (queue1 <= queue2) << std::endl;
    os << "\n[ft_queue]: After operator<= " << (ft_queue1 <= ft_queue2) << std::endl;
    if ((queue1 <= queue2) != (ft_queue1 <= ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }
    
    os << "\n[queue]: After operator> " << (queue1 > queue2) << std::endl;
    os << "\n[ft_queue]: After operator> " << (ft_queue1 > ft_queue2) << std::endl;
    if ((queue1 > queue2) != (ft_queue1 > ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator>= " << (queue1 >= queue2) << std::endl;
    os << "\n[ft_queue]: After operator>= " << (ft_queue1 >= ft_queue2) << std::endl;
    if ((queue1 >= queue2) != (ft_queue1 >= ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty queue with list -> data_a data_b\n";

    for (size_t i = 0; i < 5; i++)
    {
        queue1.push(data_a);
        ft_queue1.push(data_a);
    }

    for (size_t i = 0; i < 5; i++)
    {
        queue2.push(data_b);
        ft_queue2.push(data_b);
    }

    os << "\n[queue]: After operator== " << (queue1 == queue2) << std::endl;
    os << "\n[ft_queue]: After operator== " << (ft_queue1 == ft_queue2) << std::endl;
    if ((queue1 == queue2) != (ft_queue1 == ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator!= " << (queue1 != queue2) << std::endl;
    os << "\n[ft_queue]: After operator!= " << (ft_queue1 != ft_queue2) << std::endl;
    if ((queue1 != queue2) != (ft_queue1 != ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator< " << (queue1 < queue2) << std::endl;
    os << "\n[ft_queue]: After operator< " << (ft_queue1 < ft_queue2) << std::endl;
    if ((queue1 < queue2) != (ft_queue1 < ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator<= " << (queue1 <= queue2) << std::endl;
    os << "\n[ft_queue]: After operator<= " << (ft_queue1 <= ft_queue2) << std::endl;
    if ((queue1 <= queue2) != (ft_queue1 <= ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }
    
    os << "\n[queue]: After operator> " << (queue1 > queue2) << std::endl;
    os << "\n[ft_queue]: After operator> " << (ft_queue1 > ft_queue2) << std::endl;
    if ((queue1 > queue2) != (ft_queue1 > ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[queue]: After operator>= " << (queue1 >= queue2) << std::endl;
    os << "\n[ft_queue]: After operator>= " << (ft_queue1 >= ft_queue2) << std::endl;
    if ((queue1 >= queue2) != (ft_queue1 >= ft_queue2))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}


template <typename T>
void queue_run(std::ofstream* out, T (*f)())
{
    std::cout << BLUE << "\n[check constructor]: \t" << ((queue_check_constructor<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check empty]: \t\t" << ((queue_check_empty<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check size]: \t\t" << ((queue_check_size<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check front]: \t\t" << ((queue_check_front<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check back]: \t\t" << ((queue_check_back<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check pop]: \t\t" << ((queue_check_pop<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check push]: \t\t" << ((queue_check_push<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check relational]: \t" << ((queue_check_relational<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << RESET << std::endl;
}

template <typename T>
void queue_launcher(std::ofstream* p_log)
{
    if (typeid(T) == typeid(int))
        queue_run<int>(p_log, int_generator);
    else if (typeid(T) == typeid(float))
        queue_run<float>(p_log, float_generator);
    else if (typeid(T) == typeid(double))
        queue_run<double>(p_log, double_generator);
    else if (typeid(T) == typeid(char))
        queue_run<char>(p_log, char_generator);
    else if (typeid(T) == typeid(std::string))
        queue_run<std::string>(p_log, string_generator);
}

#endif
