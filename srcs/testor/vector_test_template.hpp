#ifndef VECTOR_TEST_TEMPLATE_HPP
#define VECTOR_TEST_TEMPLATE_HPP

#include "../main.hpp"

/*** LOGS PRINT ***/
template <typename T>
void print_vector(std::stringstream& os, std::vector<T>& vec, ft::vector<T>& ft_vec)
{
    typename std::vector<T>::iterator it = vec.begin();
    typename std::vector<T>::iterator ite = vec.end();

    typename ft::vector<T>::iterator ft_it = ft_vec.begin();
    typename ft::vector<T>::iterator ft_ite = ft_vec.end();

    os << "vector :\t\t";
    for (; it != ite ; ++it)
        os << *it << " ";
    os << std::endl;

    os << "ft_vector :\t\t";
    for (; ft_it != ft_ite ; ++ft_it)
        os << *ft_it << " ";
    os << std::endl;

    typename std::vector<T>::reverse_iterator r_it = vec.rbegin();
    typename std::vector<T>::reverse_iterator r_ite = vec.rend();
    typename ft::vector<T>::reverse_iterator ft_r_it = ft_vec.rbegin();
    typename ft::vector<T>::reverse_iterator ft_r_ite = ft_vec.rend();

    os << "vector (reverse) :\t";
    for (; r_it != r_ite ; ++r_it)
        os << *r_it << " ";
    os << std::endl;

    os << "ft_vector (reverse) :\t";
    for (; ft_r_it != ft_r_ite ; ++ft_r_it)
        os << *ft_r_it << " ";
    os << std::endl;
}

template <typename T>
void print_size(std::stringstream& os, std::vector<T>& vec, ft::vector<T>& ft_vec)
{
    os << "vector.size = \t\t" << vec.size() << std::endl;
    os << "ft_vector.size = \t" << ft_vec.size() << std::endl;

    os << "vector.capacity = \t" << vec.capacity() << std::endl;
    os << "ft_vector.capacity = \t" << ft_vec.capacity() << std::endl;
}

/*** CHECK VECTOR ***/
template <typename T>
bool check_vector(std::vector<T>& vec, ft::vector<T>& ft_vec)
{
    typename std::vector<T>::iterator it = vec.begin();
    typename std::vector<T>::iterator ite = vec.end();
    typename ft::vector<T>::iterator ft_it = ft_vec.begin();
    typename ft::vector<T>::iterator ft_ite = ft_vec.end();

    for (; it != ite && ft_it != ft_ite ; ++it, ++ft_it)
        if (*it != *ft_it)
            return (false);
    
    typename std::vector<T>::reverse_iterator r_it = vec.rbegin();
    typename std::vector<T>::reverse_iterator r_ite = vec.rend();
    typename ft::vector<T>::reverse_iterator ft_r_it = ft_vec.rbegin();
    typename ft::vector<T>::reverse_iterator ft_r_ite = ft_vec.rend();

    for (; r_it != r_ite && ft_r_it != ft_r_ite ; ++r_it, ++ft_r_it)
        if (*r_it != *ft_r_it)
            return (false);

    if (vec.size() != ft_vec.size())
        return (false);

    return (true);
}

/*** CHECK CONSTRUCTORS ***/
template <typename T>
bool vector_check_constructor (std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_constructor : \n" << RESET;
    os << "\n~ Empty constructor:\n\n";

    std::vector<T>  vec;
    ft::vector<T>   ft_vec;
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ Constructor (size_type n, const value_type& val) :\n\n";
    std::vector<T>  vec1(4, data_a);
    ft::vector<T>   ft_vec1(4, data_a);

    print_vector(os, vec1, ft_vec1);
    print_size(os, vec1, ft_vec1);

    if (check_vector(vec1, ft_vec1) == false)
    {
        *p_log << os.str();
        return (false);
    }


    typename std::vector<T>::iterator it = vec1.begin();
    typename std::vector<T>::iterator ite = vec1.end();
    typename ft::vector<T>::iterator ft_it = ft_vec1.begin();
    typename ft::vector<T>::iterator ft_ite = ft_vec1.end();

    os << "\n~ Constructor (iterator first, iterator last) :\n\n";
    std::vector<T>  vec2(it, ite);
    ft::vector<T>   ft_vec2(ft_it, ft_ite);

    print_vector(os, vec2, ft_vec2);
    print_size(os, vec2, ft_vec2);

    if (check_vector(vec2, ft_vec2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::vector<T>::const_iterator it1 = vec2.begin();
    typename std::vector<T>::const_iterator ite1 = vec2.end();
    typename ft::vector<T>::const_iterator ft_it1 = ft_vec2.begin();
    typename ft::vector<T>::const_iterator ft_ite1 = ft_vec2.end();

    os << "\n~ Constructor (const_iterator first, const_iterator last) :\n\n";
    std::vector<T>  vec3(it1, ite1);
    ft::vector<T>   ft_vec3(ft_it1, ft_ite1);

    print_vector(os, vec3, ft_vec3);
    print_size(os, vec3, ft_vec3);

    if (check_vector(vec3, ft_vec3) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ Constructor (const vector& x) :\n\n";
    std::vector<T>  vec4(vec1);
    ft::vector<T>   ft_vec4(ft_vec1);
    print_vector(os, vec4, ft_vec4);
    print_size(os, vec4, ft_vec4);

    if (check_vector(vec4, ft_vec4) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** CHECK OPERATOR ***/
template <typename T>
bool vector_check_operator(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_operator : \n" << RESET;
    os << "\n~ vector& operator = (const vector& assignObj); :\n\n";

    std::vector<T> vec(8, data_a);
    ft::vector<T> ft_vec(8, data_a);
    std::vector<T> vec2(6, data_b);
    ft::vector<T> ft_vec2(6, data_b);

    os << "\nBefore use operator overloading:\n";
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }
    
    os << "\nBefore use operator overloading :\n";
    print_vector(os, vec2, ft_vec2);
    print_size(os, vec2, ft_vec2);

    if (check_vector(vec2, ft_vec2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    vec = vec2;
    ft_vec = ft_vec2;

    os << "\nAfter use operator overloading :\n";
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter use operator overloading :\n";
    print_vector(os, vec2, ft_vec2);
    print_size(os, vec2, ft_vec2);


    if (check_vector(vec2, ft_vec2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** BEGIN ***/
template <typename T>
bool vector_check_begin(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_begin : \n" << RESET;
    os << "\n~ x.begin() :\n\n";

    std::vector<T> vec(5, data_a);
    ft::vector<T> ft_vec(5, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    std::vector<T> vec2(10, data_b);
    ft::vector<T> ft_vec2(10, data_b);

    print_vector(os, vec2, ft_vec2);
    print_size(os, vec2, ft_vec2);

    if (check_vector(vec2, ft_vec2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::vector<T>::iterator it = vec.begin();
    typename std::vector<T>::iterator ite = vec.end();
    typename ft::vector<T>::iterator ft_it = ft_vec.begin();
    typename ft::vector<T>::iterator ft_ite = ft_vec.end();

    typename std::vector<T>::const_iterator it1 = vec.begin();
    typename std::vector<T>::const_iterator ite1 = vec.end();
    typename ft::vector<T>::const_iterator ft_it1 = ft_vec.begin();
    typename ft::vector<T>::const_iterator ft_ite1 = ft_vec.end();

    typename std::vector<T>::iterator it2 = vec2.begin();
    typename std::vector<T>::iterator ite2 = vec2.end();
    typename ft::vector<T>::iterator ft_it2 = ft_vec2.begin();
    typename ft::vector<T>::iterator ft_ite2 = ft_vec2.end();

    typename std::vector<T>::const_iterator it3 = vec2.begin();
    typename std::vector<T>::const_iterator ite3 = vec2.end();
    typename ft::vector<T>::const_iterator ft_it3 = ft_vec2.begin();
    typename ft::vector<T>::const_iterator ft_ite3 = ft_vec2.end();

    if (!check_iterators<typename std::vector<T>::iterator, typename ft::vector<T>::iterator>(it, ite, ft_it, ft_ite) ||
    !check_iterators<typename std::vector<T>::const_iterator, typename ft::vector<T>::const_iterator>(it1, ite1, ft_it1, ft_ite1) ||
    !check_iterators<typename std::vector<T>::iterator, typename ft::vector<T>::iterator>(it2, ite2, ft_it2, ft_ite2) ||
    !check_iterators<typename std::vector<T>::const_iterator, typename ft::vector<T>::const_iterator>(it3, ite3, ft_it3, ft_ite3))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

/*** RBEGIN ***/
template <typename T>
bool vector_check_rbegin(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_rbegin : \n" << RESET;
    os << "\n~ x.rbegin() :\n\n";

    std::vector<T> vec(5, data_a);
    ft::vector<T> ft_vec(5, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n";
    std::vector<T> vec2(10, data_b);
    ft::vector<T> ft_vec2(10, data_b);

    print_vector(os, vec2, ft_vec2);
    print_size(os, vec2, ft_vec2);

    if (check_vector(vec2, ft_vec2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::vector<T>::reverse_iterator it = vec.rbegin();
    typename std::vector<T>::reverse_iterator ite = vec.rend();
    typename ft::vector<T>::reverse_iterator ft_it = ft_vec.rbegin();
    typename ft::vector<T>::reverse_iterator ft_ite = ft_vec.rend();

    typename std::vector<T>::const_reverse_iterator it1 = vec.rbegin();
    typename std::vector<T>::const_reverse_iterator ite1 = vec.rend();
    typename ft::vector<T>::const_reverse_iterator ft_it1 = ft_vec.rbegin();
    typename ft::vector<T>::const_reverse_iterator ft_ite1 = ft_vec.rend();

    typename std::vector<T>::reverse_iterator it2 = vec2.rbegin();
    typename std::vector<T>::reverse_iterator ite2 = vec2.rend();
    typename ft::vector<T>::reverse_iterator ft_it2 = ft_vec2.rbegin();
    typename ft::vector<T>::reverse_iterator ft_ite2 = ft_vec2.rend();

    typename std::vector<T>::const_reverse_iterator it3 = vec2.rbegin();
    typename std::vector<T>::const_reverse_iterator ite3 = vec2.rend();
    typename ft::vector<T>::const_reverse_iterator ft_it3 = ft_vec2.rbegin();
    typename ft::vector<T>::const_reverse_iterator ft_ite3 = ft_vec2.rend();

    if (!check_iterators<typename std::vector<T>::reverse_iterator, typename ft::vector<T>::reverse_iterator>(it, ite, ft_it, ft_ite) ||
    !check_iterators<typename std::vector<T>::const_reverse_iterator, typename ft::vector<T>::const_reverse_iterator>(it1, ite1, ft_it1, ft_ite1) ||
    !check_iterators<typename std::vector<T>::reverse_iterator, typename ft::vector<T>::reverse_iterator>(it2, ite2, ft_it2, ft_ite2) ||
    !check_iterators<typename std::vector<T>::const_reverse_iterator, typename ft::vector<T>::const_reverse_iterator>(it3, ite3, ft_it3, ft_ite3))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

/*** MAXSIZE ***/
template <typename T>
bool vector_check_maxsize(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_maxsize : \n\n" << RESET;
    os << "~ size_type max_size() const\n\n";

    std::vector<T> vec;
    ft::vector<T> ft_vec;

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nvector.maxsize = \t" << vec.max_size() << std::endl;
    os << "ft_vector.maxsize = \t" << ft_vec.max_size() << std::endl;

    if (vec.max_size() != ft_vec.max_size())
    {
        *p_log << os.str();
        return (false);
    }

    vec.assign(5, data_a);
    ft_vec.assign(5, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

  
    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nvector.maxsize = \t" << vec.max_size() << std::endl;
    os << "ft_vector.maxsize = \t" << ft_vec.max_size() << std::endl;
    os << "\n";
    
    if (vec.max_size() != ft_vec.max_size())
    {
        *p_log << os.str();
        return (false);
    }

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);
    
    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}


/*** RESIZE ***/
template <typename T>
bool vector_check_resize(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_resize : \n\n" << RESET;
    os << "~ void resize (size_type n, value_type val = value_type());\n";
 
    os << "\nBefore resize vectors, empty:\n\n" << std::endl;

    std::vector<T> vec;
    ft::vector<T> ft_vec;

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter resize vectors :\n" << std::endl;

    vec.resize(5, data_a);
    ft_vec.resize(5, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    (void)data_a;

    *p_log << os.str();
    return (true);
}

/*** EMPTY ***/
template <typename T>
bool vector_check_empty(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_empty: \n\n" << RESET;
    os << "~ void empty();\n";
 
    os << "\nBefore, with empty vector:\n\n" << std::endl;

    std::vector<T> vec;
    ft::vector<T> ft_vec;

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nEmpty vector : \t\t" << vec.empty() << std::endl;
    os << "Empty ft_vector : \t" << ft_vec.empty() << std::endl;
    if ((vec.empty()) != (ft_vec.empty()))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter assign :\n\n" << std::endl;

    vec.assign(6, data_a);
    ft_vec.assign(6, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nEmpty vector : \t\t" << vec.empty() << std::endl;
    os << "Empty ft_vector : \t" << ft_vec.empty() << std::endl;
    if ((vec.empty()) != (ft_vec.empty()))
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** RESERVE ***/
template <typename T>
bool vector_check_reserve(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_reserve : \n\n" << RESET;
    os << "~ void reserve (size_type n);\n\n";

    os << "Before, with empty vector :\n\n";
    std::vector<T> vec;
    ft::vector<T> ft_vec;

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nReserve 200 to the same vector :\n\n";

    vec.reserve(200);
    ft_vec.reserve(200);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    (void)data_a;

    *p_log << os.str();
    return (true);
}

/*** FRONT ***/
template <typename T>
bool vector_check_front(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_front : \n\n" << RESET;
    os << "~ reference front ();\n\n";

    std::vector<T> vec(10, data_a);
    ft::vector<T> ft_vec(10, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nvector : \t\t" << vec.front() << std::endl;
    os << "ft_vector : \t\t" << ft_vec.front() << std::endl;

    if ((vec.front()) != (ft_vec.front()))
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    (void)data_a;

    *p_log << os.str();
    return (true);
}

/*** BACK ***/
template <typename T>
bool vector_check_back(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_back : \n\n" << RESET;
    os << "~ reference back ();\n\n";

    std::vector<T> vec(10, data_a);
    ft::vector<T> ft_vec(10, data_a);
    vec.push_back(data_b);
    ft_vec.push_back(data_b);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nvector : \t\t" << vec.back() << std::endl;
    os << "ft_vector : \t\t" << ft_vec.back() << std::endl;

    if ((vec.back()) != (ft_vec.back()))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

/*** OPERATOR [] ***/
template <typename T>
bool vector_check_crochet(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_crochet : \n\n" << RESET;
    os << "~ reference operator[] (size_type n);\n\n";

    std::vector<T> vec(6, data_a);
    ft::vector<T> ft_vec(6, data_a);
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nvector[] : \n";
    for (size_t i = 0 ; i < vec.size() ; i++)
        os << vec[i] << " ";
    os << "\n";

    os << "\nft_vector[] : \n";
    for (size_t i = 0 ; i < ft_vec.size() ; i++)
        os << ft_vec[i] << " ";
    os << "\n";

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** AT ***/
template <typename T>
bool vector_check_at(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_at : \n\n" << RESET;
    os << "~ reference at (size_type n);\n\n";

    std::vector<T> vec(6, data_a);
    ft::vector<T> ft_vec(6, data_a);
    vec.push_back(data_b);
    ft_vec.push_back(data_b);
    for (int i = 0 ; i < 6 ; i++)
    {
        vec.push_back(data_a);
        ft_vec.push_back(data_a);
    }
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nvector.at(6) :\t\t" << vec.at(6) << "\n";
    os << "ft_vector.at(6) :\t" << ft_vec.at(6) << "\n";

    if ((vec.at(6)) != (ft_vec.at(6)))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nTest with out of range exception 228 :\n\n";

    try
    {
        os << vec.at(228);
        *p_log << os.str();
        return (false);
    }
    catch(const std::exception& e)
    {
        os << "[vector]: Error on\n";
    }

    try
    {
        os << ft_vec.at(228);
        *p_log << os.str();
        return (false);
    }
    catch(const std::exception& e)
    {
        os << "[ft_vector]: Error on\n";
    }


    (void)data_b;
    (void)data_a;

    *p_log << os.str();
    return (true);
}

/*** ASSIGN ***/
template <typename T>
bool vector_check_assign(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_assign : \n\n" << RESET;
    os << "~ void assign (iterator first, iterator last);\n\n";

    os << "Before, with empty vector :\n\n";
    std::vector<T> vec;
    ft::vector<T> ft_vec;

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAssign 5 with data_a to the same vector :\n\n";

    vec.assign(5, data_a);
    ft_vec.assign(5, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}


/*** PUSH_BACK ***/
template <typename T>
bool vector_check_push_back(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_push_back : \n\n" << RESET;
    os << "~ void push_back (const value_type& val);\n\n";

    os << "Before:\n\n";
    std::vector<T> vec(3, data_b);
    ft::vector<T> ft_vec(3, data_b);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter, push_back(data_a) x 4 :\n\n";

    for (int i = 0 ; i < 4 ; i++)
    {
        vec.push_back(data_a);
        ft_vec.push_back(data_a);
    }
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** POP_BACK ***/
template <typename T>
bool vector_check_pop_back(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_pop_back : \n\n" << RESET;
    os << "~ void pop_back();\n\n";

    os << "Before, with 4 x data_a :\n\n";
    std::vector<T> vec(4, data_a);
    ft::vector<T> ft_vec(4, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter, pop_back() x 2 :\n\n";

    for (int i = 0 ; i < 2 ; i++)
    {
        vec.pop_back();
        ft_vec.pop_back();
    }
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}


/*** CLEAR ***/
template <typename T>
bool vector_check_clear(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_clear : \n\n" << RESET;
    os << "~ void clear();\n\n";

    os << "Before, with 4 x data_a :\n\n";
    std::vector<T> vec(4, data_a);
    ft::vector<T> ft_vec(4, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter use clear() :\n\n";

    vec.clear();
    ft_vec.clear();

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** INSERT ***/
template <typename T>
bool vector_check_insert(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_insert : \n\n" << RESET;

    os << "Before, with 4 x data_a :\n\n";
    std::vector<T> vec(4, data_a);
    ft::vector<T> ft_vec(4, data_a);

    for (size_t i = 0; i < 4 ; i++)
    {
        vec.push_back(data_b);
        ft_vec.push_back(data_b);
    }

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ void insert (iterator position, size_type n, const value_type& val);\n\n";
    
    os << "Insert data_a value to begin and data_b to end\n\n";
    typename std::vector<T>::iterator it = vec.begin();
    typename ft::vector<T>::iterator ft_it = ft_vec.begin();
    vec.insert(it, data_b);
    ft_vec.insert(ft_it, data_b);

    typename std::vector<T>::iterator ite = vec.end();
    typename ft::vector<T>::iterator ft_ite = ft_vec.end();
    vec.insert(ite, data_a);
    ft_vec.insert(ft_ite, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ void insert (iterator position, size_type n, const value_type& val);\n\n";
    it = vec.begin();
    ft_it = ft_vec.begin();
    vec.insert(it, 4, data_a);
    ft_vec.insert(ft_it, 4, data_a);

    ite = vec.begin();
    ft_ite = ft_vec.begin();
    vec.insert(it, 4, data_b);
    ft_vec.insert(ft_it, 4, data_b);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    std::vector<T> vec2(11, data_a);
    ft::vector<T> ft_vec2(11, data_a);

    it = vec2.begin();
    ite = vec2.end();
    ft_it = ft_vec2.begin();
    ft_ite = ft_vec2.end();

    os << "\nAfter run insert (iterator position, iterator first, iterator last)\n";
    vec.insert(vec.begin(), it, ite);
    ft_vec.insert(ft_vec.begin(), ft_it, ft_ite);


    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}


/*** ERASE ***/
template <typename T>
bool vector_check_erase(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_erase : \n\n" << RESET;

    os << "Before, with 4 x 1 :\n\n";
    std::vector<T> vec(4, data_a);
    ft::vector<T> ft_vec(4, data_a);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nInsert 0 x 3 to begin :\n\n";
    typename std::vector<T>::iterator it = vec.begin();
    typename ft::vector<T>::iterator ft_it = ft_vec.begin();
    vec.insert(it, 3, data_b);
    ft_vec.insert(ft_it, 3, data_b);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nNow erase data_b x 1 :\n\n";
    typename std::vector<T>::iterator it1 = vec.begin();
    typename ft::vector<T>::iterator ft_it1 = ft_vec.begin();

    vec.erase(++it1);
    ft_vec.erase(++ft_it1);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::vector<T>::iterator it2 = vec.begin();
    typename ft::vector<T>::iterator ft_it2 = ft_vec.begin();
    typename std::vector<T>::iterator ite2 = vec.end();
    typename ft::vector<T>::iterator ft_ite2 = ft_vec.end();
    
    os << "\nNow erase all the rest:\n\n";
    
    vec.erase(it2, ite2);
    ft_vec.erase(ft_it2, ft_ite2);

    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** SWAP ***/
template <typename T>
bool vector_check_swap(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_swap : \n\n" << RESET;

    os << "std::vector 1 :\t\t";
    std::vector<T> vec(4, data_a);
    for (size_t i = 0 ; i < vec.size() ; i++)
        os << vec[i] << " ";
    os << std::endl;

    os << "std::vector_swap :\t";
    std::vector<T> vec_swap(4, data_b);
    for (size_t i = 0 ; i < vec_swap.size() ; i++)
        os << vec_swap[i] << " ";
    os << std::endl;

    os << "\nAfter swap :\n\n";
    vec.swap(vec_swap);

    os << "std::vector 1 :\t\t";
    for (size_t i = 0 ; i < vec.size() ; i++)
        os << vec[i] << " ";
    os << std::endl;

    os << "std::vector_swap :\t";
    for (size_t i = 0 ; i < vec_swap.size() ; i++)
        os << vec_swap[i] << " ";
    os << std::endl;


    os << "\n\nft::vector 1 :\t\t";
    ft::vector<T> ft_vec(4, data_a);
    for (size_t i = 0 ; i < ft_vec.size() ; i++)
        os << ft_vec[i] << " ";
    os << std::endl;

    os << "ft::vector_swap :\t";
    ft::vector<T> ft_vec_swap(4, data_b);
    for (size_t i = 0 ; i < ft_vec_swap.size() ; i++)
        os << ft_vec_swap[i] << " ";
    os << std::endl;

    os << "\nAfter swap :\n\n";
    ft_vec.swap(ft_vec_swap);

    os << "ft::vector 1 :\t\t";
    for (size_t i = 0 ; i < ft_vec.size() ; i++)
        os << ft_vec[i] << " ";
    os << std::endl;

    os << "ft::vector_swap :\t";
    for (size_t i = 0 ; i < ft_vec_swap.size() ; i++)
        os << ft_vec_swap[i] << " ";
    os << std::endl;

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }

    if (vec_swap.size() != ft_vec_swap.size())
    {
        *p_log << os.str();
        return (false);
    }
    if (check_vector(vec_swap, ft_vec_swap) == false)
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

/*** RELATIONAL ***/
template <typename T>
bool vector_check_relational(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 vector_check_relational : \n\n" << RESET;
    
    std::vector<T> vec(4, data_a);
    ft::vector<T> ft_vec(4, data_a);
    print_vector(os, vec, ft_vec);
    print_size(os, vec, ft_vec);

    if (check_vector(vec, ft_vec) == false)
    {
        *p_log << os.str();
        return (false);
    }
    os << "\n";
    std::vector<T> vec1(4, data_b);
    ft::vector<T> ft_vec1(4, data_b);
    print_vector(os, vec1, ft_vec1);
    print_size(os, vec1, ft_vec1);

    if (vec1.size() != ft_vec1.size())
    {
        *p_log << os.str();
        return (false);
    }
    if (check_vector(vec1, ft_vec1) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[vector]: After operator ==\t" << (vec == vec1) << std::endl;
    os << "[ft_vector]: After operator ==\t" << (ft_vec == ft_vec1) << std::endl;
    if ((vec == vec1) != (ft_vec == ft_vec1))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[vec]: After operator !=\t" << (vec != vec1) << std::endl;
    os << "[ft_vec]: After operator !=\t" << (ft_vec != ft_vec1) << std::endl;
    if ((vec != vec1) != (ft_vec != ft_vec1))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[vec]: After operator <\t\t" << (vec < vec1) << std::endl;
    os << "[ft_vec]: After operator <\t" << (ft_vec < ft_vec1) << std::endl;
    if ((vec < vec1) != (ft_vec < ft_vec1))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[vec]: After operator <=\t" << (vec <= vec1) << std::endl;
    os << "[ft_vec]: After operator <=\t" << (ft_vec <= ft_vec1) << std::endl;
    if ((vec <= vec1) != (ft_vec <= ft_vec1))
    {
        *p_log << os.str();
        return (false);
    }
    
    os << "\n[vec]: After operator >\t\t" << (vec > vec1) << std::endl;
    os << "[ft_vec]: After operator >\t" << (ft_vec > ft_vec1) << std::endl;
    if ((vec > vec1) != (ft_vec > ft_vec1))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[vec]: After operator >=\t" << (vec >= vec1) << std::endl;
    os << "[ft_vec]: After operator >=\t" << (ft_vec >= ft_vec1) << std::endl;
    if ((vec >= vec1) != (ft_vec >= ft_vec1))
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}


/*** LAUNCHER ***/
template <typename T>
void vector_run(std::ofstream* p_log, T (*f)())
{
    std::cout << BLUE << "\n[check constructor]: \t" << ((vector_check_constructor<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check operator =]: \t" << ((vector_check_operator<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check begin - end]: \t" << ((vector_check_begin<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check rbegin - rend]: \t" << ((vector_check_rbegin<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check maxisze]: \t" << ((vector_check_maxsize<T>(p_log, f(), f())) ? GREEN"OK" : YELLOW"DEPEND") << RESET;
    std::cout << BLUE << "\n[check resize]: \t" << ((vector_check_resize<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check empty]: \t\t" << ((vector_check_empty<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check reserve]: \t" << ((vector_check_reserve<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check front]: \t\t" << ((vector_check_front<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check back]: \t\t" << ((vector_check_back<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check crochet]: \t" << ((vector_check_crochet<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check at]: \t\t" << ((vector_check_at<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check assign]: \t" << ((vector_check_assign<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check push_back]: \t" << ((vector_check_push_back<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check pop_back]: \t" << ((vector_check_pop_back<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check clear]: \t\t" << ((vector_check_clear<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check insert]: \t" << ((vector_check_insert<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check erase]: \t\t" << ((vector_check_erase<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check swap]: \t\t" << ((vector_check_swap<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check relational]: \t" << ((vector_check_relational<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << "\n\n";
}

template <typename T>
void vector_launcher(std::ofstream* p_log)
{
    if (typeid(T) == typeid(int))
        vector_run<int>(p_log, int_generator);
    else if (typeid(T) == typeid(float))
        vector_run<float>(p_log, float_generator);
    else if (typeid(T) == typeid(double))
        vector_run<double>(p_log, double_generator);
    else if (typeid(T) == typeid(char))
        vector_run<char>(p_log, char_generator);
    else if (typeid(T) == typeid(std::string))
        vector_run<std::string>(p_log, string_generator);
}

#endif
