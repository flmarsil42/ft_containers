CC = 			clang++

FLAGS = 		-Wall -Werror -Wextra -std=c++98

CONTAINERS =	$(addprefix -I includes/Containers/, List.hpp Queue.hpp Vector.hpp Map.hpp Stack.hpp)

ITERATORS = 	$(addprefix -I includes/Iterators/, Bidirectional.hpp RandomAccess.hpp Node.hpp)

TEMPLATES = 	$(addprefix -I includes/Templates/, Allocator.hpp TypeConst.hpp main.hpp)

SRCS =			$(addprefix srcs/, main.cpp )

TESTS =			$(addprefix srcs/testor/, vector_test_template.hpp list_test_template.hpp \
							map_test_template.hpp stack_test_template.hpp queue_test_template.hpp)

OBJS = 			$(SRCS:.cpp=.o)

$(OBJS) = 		$(CONTAINERS) $(ITERATORS) $(TEMPLATES) $(TESTS)

NAME = 			testor.out

$(NAME): $(OBJS) $(SRCS)
	@ $(CC) $(FLAGS) $(OBJS) -o $@

%.o : %.cpp
	@ $(CC) $(FLAGS) -c -o $@ $<

all : 		$(NAME)

clean :
			@ rm -f $(OBJS)

fclean : 	clean
			@ rm -f $(NAME)
			@ rm -f ./srcs/testor/logs/*

re : 		fclean all
